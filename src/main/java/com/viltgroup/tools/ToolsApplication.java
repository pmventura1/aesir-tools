package com.viltgroup.tools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.viltgroup.tools.data.GenerateIconTheme;

@SpringBootApplication
public class ToolsApplication implements CommandLineRunner {

	@Autowired
	private GenerateIconTheme task;
	
	
	public static void main(String[] args) {
		SpringApplication.run(ToolsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		task.execute();
		
	}

}
