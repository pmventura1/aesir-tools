package com.viltgroup.tools.data;

import java.util.ArrayList;
import java.util.List;

public class Type {
	
	private String file;
	
	
	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	private String name;
	
	private List<String> indexes = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getIndexes() {
		return indexes;
	}

	public void setIndexes(List<String> indexes) {
		this.indexes = indexes;
	}
	
	
	
	

}
