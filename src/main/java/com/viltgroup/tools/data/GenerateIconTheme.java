package com.viltgroup.tools.data;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.viltgroup.tools.tasks.Task;
import com.viltgroup.tools.tasks.exception.TaskExecutionException;

@Service
public class GenerateIconTheme implements Task {

	private static final File BASE_DIR = new File("E:\\work\\Aesir\\assets\\icons");
	private static final File OUTPUT_DIR = new File("E:\\work\\Aesir\\assets\\icons\\theme");

	private static final String WRONG_GOLD = "#f2b400";
	private static final String GOLD = "#F1B41C";
	private static final String DARK = "#222222";
	private static final String LIGHT = "#CCCCCC";
	private static final String WHITE = "#FFFFFF";
	private static final String GRAY = "#666666";
	

	@Override
	public void execute() throws TaskExecutionException {

		FileUtils.deleteQuietly(OUTPUT_DIR);

		OUTPUT_DIR.mkdir();

		Collection<File> svgFiles = FileUtils.listFiles(BASE_DIR, new String[] { "svg" }, false);

		Path outputPath = Path.of(OUTPUT_DIR.getPath());
		Path basePath = Path.of(BASE_DIR.getPath());

		List<String> cssRules = new ArrayList<String>();

		try {
			for (File svg : svgFiles) {

				Path path = Path.of(svg.getPath());

				List<String> lines = Files.readAllLines(path);

				List<String> goldLines = lines.stream().map(line -> line.replaceAll("(?i)" + WRONG_GOLD, GOLD))
						.collect(Collectors.toList());

				List<String> darkLines = goldLines.stream().map(line -> line.replaceAll("(?i)" + GOLD, DARK))
						.collect(Collectors.toList());

				List<String> lightLines = goldLines.stream().map(line -> line.replaceAll("(?i)" + GOLD, LIGHT))
						.collect(Collectors.toList());
				
				List<String> whiteLines = goldLines.stream().map(line -> line.replaceAll("(?i)" + GOLD, WHITE))
						.collect(Collectors.toList());
				
				List<String> grayLines = goldLines.stream().map(line -> line.replaceAll("(?i)" + GOLD, GRAY))
						.collect(Collectors.toList());

				String name = FilenameUtils.getBaseName(path.toString());

				Path goldIcon = outputPath.resolve(name + "-gold.svg");
				Path darkIcon = outputPath.resolve(name + "-dark.svg");
				Path lightIcon = outputPath.resolve(name + "-light.svg");
				Path whiteIcon = outputPath.resolve(name + "-white.svg");
				Path grayIcon = outputPath.resolve(name + "-gray.svg");

				Files.write(goldIcon, goldLines, Charset.forName("UTF-8"));
				Files.write(darkIcon, darkLines, Charset.forName("UTF-8"));
				Files.write(lightIcon, lightLines, Charset.forName("UTF-8"));
				Files.write(whiteIcon, whiteLines, Charset.forName("UTF-8"));
				Files.write(grayIcon, grayLines, Charset.forName("UTF-8"));

				cssRules.add("");
				cssRules.add(".icon-" + name + "{");
				cssRules.add("  background-image: url('" + goldIcon.getFileName() + "');");
				cssRules.add("}");
				
				cssRules.add("");
				cssRules.add(".icon-" + name + "-dark{");
				cssRules.add("  background-image: url('" + darkIcon.getFileName() + "');");
				cssRules.add("}");

				cssRules.add("");
				cssRules.add(".icon-" + name + "-light{");
				cssRules.add("  background-image: url('" + lightIcon.getFileName() + "');");
				cssRules.add("}");
				
				
				
				cssRules.add("");
				cssRules.add(".icon-" + name + "-white{");
				cssRules.add("  background-image: url('" + whiteIcon.getFileName() + "');");
				cssRules.add("}");
				
				
				cssRules.add("");
				cssRules.add(".icon-" + name + "-gray{");
				cssRules.add("  background-image: url('" + grayIcon.getFileName() + "');");
				cssRules.add("}");

				/** Remove circle **/
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();

				Document xmlDoc = dbf.newDocumentBuilder().parse(goldIcon.toFile());
				
				
//				XPath xpath = XPathFactory.newInstance().newXPath();
//				Node nodeToRemove = (Node) xpath.evaluate("//*[@id='path4']", xmlDoc, XPathConstants.NODE);

//				.map(s -> s.replaceFirst("viewBox=\"0 0 350 350\"", "viewBox=\"0 0 220 220\"").replaceFirst("height=\"350\"", "height=\"220\"").replaceFirst("width=\"350\"", "width=\"220\"") )
				List<String> simpleGoldLines  = IntStream.range(0, goldLines.size()).filter(i -> i<52 || i > 58).mapToObj(i -> goldLines.get(i)).collect(Collectors.toList());
				List<String> simpleDarkLines  = IntStream.range(0, darkLines.size()).filter(i -> i<52 || i > 58).mapToObj(i -> darkLines.get(i)).collect(Collectors.toList());
				List<String> simpleLightLines  = IntStream.range(0, lightLines.size()).filter(i -> i<52 || i > 58).mapToObj(i -> lightLines.get(i)).collect(Collectors.toList());
				List<String> simpleWhiteLines  = IntStream.range(0, whiteLines.size()).filter(i -> i<52 || i > 58).mapToObj(i -> whiteLines.get(i)).collect(Collectors.toList());
				List<String> simpleGrayLines  = IntStream.range(0, grayLines.size()).filter(i -> i<52 || i > 58).mapToObj(i -> grayLines.get(i)).collect(Collectors.toList());
				
				Path simpleGoldIcon = outputPath.resolve(name + "-simple-gold.svg");
				Path simpleDarkIcon = outputPath.resolve(name + "-simple-dark.svg");
				Path simpleLightIcon = outputPath.resolve(name + "-simple-light.svg");
				Path simpleWhiteIcon = outputPath.resolve(name + "-simple-white.svg");
				Path simpleGrayIcon = outputPath.resolve(name + "-simple-gray.svg");
			    
				Files.write(simpleGoldIcon, simpleGoldLines, Charset.forName("UTF-8"));
				Files.write(simpleDarkIcon, simpleDarkLines, Charset.forName("UTF-8"));
				Files.write(simpleLightIcon, simpleLightLines, Charset.forName("UTF-8"));
				Files.write(simpleWhiteIcon, simpleWhiteLines, Charset.forName("UTF-8"));
				Files.write(simpleGrayIcon, simpleGrayLines, Charset.forName("UTF-8"));
				
//				Node nodeToRemove = xmlDoc.getElementById("path4");
//				nodeToRemove.getParentNode().removeChild(nodeToRemove);
//				File simpleGoldFile = outputPath.resolve(name + "-simple-gold.svg").toFile();
//				java.io.Writer writer = new java.io.FileWriter(simpleGoldFile);
//				
//				final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
//			    final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
//			    final LSSerializer xml = impl.createLSSerializer();
//			    LSOutput lsOutput = impl.createLSOutput();
//			    lsOutput.setEncoding("UTF-8");
//			    lsOutput.setCharacterStream(writer);
//			    
//				xml.write(xmlDoc, lsOutput);
//				
//				
//				writer.close();
				
				
				cssRules.add("");
				cssRules.add(".icon-" + name + "-simple{");
				cssRules.add("  background-image: url('" + simpleGoldIcon.getFileName() + "');");
				cssRules.add("  transform: scale(1.4);");
				cssRules.add("}");
				
				cssRules.add("");
				cssRules.add(".icon-" + name + "-simple-dark{");
				cssRules.add("  background-image: url('" + simpleDarkIcon.getFileName() + "');");
				cssRules.add("  transform: scale(1.4);");
				cssRules.add("}");
				
				
				cssRules.add("");
				cssRules.add(".icon-" + name + "-simple-light{");
				cssRules.add("  background-image: url('" + simpleLightIcon.getFileName() + "');");
				cssRules.add("  transform: scale(1.4);");
				cssRules.add("}");
				
				
				cssRules.add("");
				cssRules.add(".icon-" + name + "-simple-white{");
				cssRules.add("  background-image: url('" + simpleWhiteIcon.getFileName() + "');");
				cssRules.add("  transform: scale(1.4);");
				cssRules.add("}");
				
				
				cssRules.add("");
				cssRules.add(".icon-" + name + "-simple-gray{");
				cssRules.add("  background-image: url('" + simpleGrayIcon.getFileName() + "');");
				cssRules.add("  transform: scale(1.4);");
				cssRules.add("}");

			}

			Path baseCSS = basePath.resolve("base.css");
			Path resultCSS = outputPath.resolve("icons.css");

			Files.copy(baseCSS, resultCSS);

			Files.write(resultCSS, cssRules, Charset.forName("UTF-8"), StandardOpenOption.APPEND);

		} catch (IOException | SAXException | ParserConfigurationException e) {
			throw new TaskExecutionException(e);
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InstantiationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
		} catch (ClassCastException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//		} catch (XPathExpressionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
		}

	}

}
