package com.viltgroup.tools.tasks;

import com.viltgroup.tools.tasks.exception.TaskExecutionException;

public interface Task {
	
	
	void execute() throws TaskExecutionException;

}
