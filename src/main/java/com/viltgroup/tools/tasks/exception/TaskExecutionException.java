package com.viltgroup.tools.tasks.exception;

import org.slf4j.helpers.MessageFormatter;

public class TaskExecutionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TaskExecutionException(String pattern, Object... parameters) {
		super(MessageFormatter.arrayFormat(pattern, parameters).getMessage());
	}
	
	public TaskExecutionException(Throwable t, String pattern, Object... parameters) {
		super(MessageFormatter.arrayFormat(pattern, parameters).getMessage(), t);
	}
	
	public TaskExecutionException(Throwable t) {
		super(t);
	}

}
